module Render exposing (..)

import Element exposing (..)
import Element.Background as Background
import Element.Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (Html)
import Loading
    exposing
        ( LoaderType(..)
        , defaultConfig
        )
import Model exposing (..)


render_ui : Model -> Element Msg
render_ui model =
    case model.mode of
        Default ->
            render_default model

        Reveal ->
            render_reveal model


render_header : Model -> Element Msg
render_header model =
    case model.mode of
        Default ->
            row
                [ Region.heading 1
                , alignLeft
                , Font.size 42
                , paddingXY 36 24
                , Font.bold
                , width fill
                ]
                [ text "GENDER-SCANNER"

                -- , Input.button
                --     [ Background.color (rgb 0.4 0.2 0.6)
                --     , padding 10
                --     , Element.Border.rounded 6
                --     , alignRight
                --     , Font.size 18
                --     ]
                --     { onPress = Just ModeSwitch
                --     , label = Element.text "Revealer"
                --     }
                ]

        Reveal ->
            row
                [ Region.heading 1
                , alignLeft
                , Font.size 42
                , paddingXY 36 24
                , Font.bold
                , width fill
                ]
                [ text "GENDER-REVEALER"

                -- , Input.button
                --     [ Background.color (rgb 0.4 0.2 0.6)
                --     , padding 10
                --     , Element.Border.rounded 6
                --     , alignRight
                --     , Font.size 18
                --     ]
                --     { onPress = Just ModeSwitch
                --     , label = Element.text "Scanner"
                --     }
                ]


render_default : Model -> Element Msg
render_default model =
    column []
        [ text
            ("We have analyzed "
                ++ String.fromInt model.points
                ++ " unique data points"
            )
        , text "\tto determine that your gender is: "
        , render_gender model
        , el [ paddingXY 0 18 ]
            (text ("We estimate this determination to be 99." ++ String.fromInt (Basics.round (toFloat model.points / 2 - 920 + 429)) ++ "% accurate."))
        ]


render_reveal : Model -> Element Msg
render_reveal model =
    render_quiz model.form model.list


render_gender : Model -> Element Msg
render_gender model =
    case model.gender of
        Just g ->
            el
                [ Font.size 36
                , Font.bold
                , Font.center
                , width fill
                , height (px 50)
                , paddingXY 0 10
                , Font.unitalicized
                ]
                (text (String.toUpper ("\"" ++ g ++ "\"")))

        Nothing ->
            column
                [ centerX
                , centerY
                , width fill
                , height (px 50)
                ]
                [ el
                    [ centerX
                    , centerY
                    , width fill
                    ]
                    (Element.html
                        (Loading.render
                            Bars
                            -- LoaderType
                            { defaultConfig | color = "#999" }
                            -- Config
                            Loading.On
                         -- LoadingState
                        )
                    )
                , render_loading_message model
                ]


render_loading_message : Model -> Element Msg
render_loading_message model =
    case model.loading_message of
        Just m ->
            el
                [ width fill
                , centerX
                , Font.center
                , Font.size 12
                ]
                (text m)

        Nothing ->
            row [] []


selectOption : String -> Html Msg
selectOption opt =
    Html.option
        []
        [ Html.text opt ]


render_quiz : Form -> List String -> Element Msg
render_quiz form genders =
    column
        [ width fill
        , spacing 18
        , padding 40
        ]
        [ text "Which of these best describes your baby's current state:"
        , Input.checkbox []
            { checked = form.born
            , onChange = \new -> UpdateForm { form | born = new }
            , icon = Input.defaultCheckbox
            , label = Input.labelRight [] (text "Born")
            }
        , Input.checkbox
            []
            { checked = form.unborn
            , onChange = \new -> UpdateForm { form | unborn = new }
            , icon = Input.defaultCheckbox
            , label = Input.labelRight [] (text "Not yet born")
            }
        , Input.checkbox []
            { checked = form.unconceived
            , onChange = \new -> UpdateForm { form | unconceived = new }
            , icon = Input.defaultCheckbox
            , label = Input.labelRight [] (text "Not yet conceived")
            }
        , text "Select the Gender you most identify with right now:"
        , el []
            (Element.html
                (Html.select []
                    (genders
                        |> List.map selectOption
                    )
                )
            )
        , text
            "If you have a partner, select their gender:"
        , el []
            (Element.html
                (Html.select []
                    [ Html.option
                        []
                        [ Html.text "You can never truly know another's gender" ]
                    ]
                )
            )
        , Input.radio
            [ spacing 12
            , Background.color (rgba 0 0 0 1)
            , Font.size 18
            ]
            { selected = form.what
            , onChange = \new -> UpdateForm { form | what = Just new }
            , label = Input.labelAbove [ Font.size 24, paddingXY 0 12 ] (text "What is gender?")
            , options =
                [ Input.option A (text "Gender Is A Social Construct.")
                , Input.option Social (text "Gender Is A Social Construct.")
                , Input.option Construct (text "Gender Is A Social Construct.")
                ]
            }
        , Input.button
            [ Background.color (rgb 0.4 0.2 0.6)
            , padding
                10
            , Element.Border.rounded
                6
            , alignRight
            ]
            { onPress = Just FormSubmit
            , label = Element.text "Submit"
            }
        ]
