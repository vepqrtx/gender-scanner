module Main exposing (init, main, update, view)

import Browser
import Element exposing (..)
import Element.Background as Background
import Element.Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (Html)
import Http
import Model exposing (..)
import Process
import Random
import Render exposing (..)
import String
import Task
import Url.Parser exposing ((</>), (<?>), Parser, int, map, oneOf, s, string)
import Url.Parser.Query as Query


main =
    Browser.document
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


init : () -> ( Model, Cmd Msg )
init () =
    ( { mode = Default
      , gender = Nothing
      , points = 0
      , list = []
      , loading_message = Nothing
      , loading_counter = 0
      , form = defaultForm
      }
    , newpoints
    )


defaultForm =
    { what = Nothing
    , which = Nothing
    , born = False
    , unborn = False
    , unconceived = False
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ModeSwitch ->
            case model.mode of
                Default ->
                    ( { model | mode = Reveal }, Cmd.none )

                Reveal ->
                    ( { model | mode = Default }, Cmd.none )

        Update new_model ->
            ( new_model, Cmd.none )

        UpdateForm form ->
            ( { model | form = form }, Cmd.none )

        FormSubmit ->
            ( model, newpoints )

        NewGender gender ->
            ( { model | gender = gender }
            , Cmd.none
            )

        NewPoints p ->
            ( { model | points = p }
            , getgenders
            )

        Load ->
            if model.loading_counter >= 10 then
                ( { model | loading_counter = 0 }, selectgender model.list )

            else
                ( { model | loading_counter = model.loading_counter + 1 }, selectloadingmessage )

        SetLoadingMessage i ->
            ( { model | loading_message = loading_message i }, sleep (toFloat model.points / 3 / 10) )

        GetGenders ->
            ( model, getgenders )

        GotGenders genders ->
            case genders of
                Ok text ->
                    ( { model
                        | list = String.split "\n" text
                      }
                    , sleep (toFloat model.points / 3 / 10)
                    )

                Err _ ->
                    ( model, Cmd.none )

        GenderSelected selection ->
            ( { model
                | gender = getgender selection model
              }
            , Cmd.none
            )

        NoAction ->
            ( model, Cmd.none )


view : Model -> Browser.Document Msg
view model =
    { title = "GENDER-SCANNER"
    , body = [ body model ]
    }


body : Model -> Html Msg
body model =
    Element.layout
        [ Background.color (rgba 0 0 0 1)
        , width fill
        , height fill
        , Font.color (rgba 1 1 1 1)
        , Font.italic
        , Font.size 32
        , Font.family
            [ Font.external
                { url = "/fonts.css"
                , name = "EB Garamond"
                }
            , Font.sansSerif
            ]
        ]
    <|
        Element.column
            [ centerX, centerY, width (px 600) ]
            [ render_header model
            , render_ui model
            , Element.paragraph
                [ padding 24
                , Font.size 24
                , Font.unitalicized
                ]
                [ text "About: This website uses the latest in AI technology, assessing a variety of datapoints and analyzing them via a complex recurrant neural networks, LTSM networks, and The Cloud™, to determine your gender with a high degree of accuracy. "
                ]
            ]


sleep : Float -> Cmd Msg
sleep milliseconds =
    Process.sleep milliseconds |> Task.perform (always Load)


newpoints : Cmd Msg
newpoints =
    Random.generate NewPoints (Random.int 2000 10000)


getgenders : Cmd Msg
getgenders =
    Http.get
        { url = "nounlist.txt"
        , expect = Http.expectString GotGenders
        }


selectgender : List String -> Cmd Msg
selectgender list =
    Random.generate GenderSelected
        (Random.int 0 (List.length list - 1))


getgender : Int -> Model -> Gender
getgender selection model =
    List.drop selection model.list |> List.head


selectloadingmessage : Cmd Msg
selectloadingmessage =
    Random.generate SetLoadingMessage
        (Random.int 0 (List.length loading_messages - 1))


loading_message : Int -> Maybe String
loading_message index =
    List.drop index loading_messages |> List.head


loading_messages =
    [ "Reticulating Splines"
    , "Calculating Gender Hash"
    , "Analyzing Facial Recognition Data"
    , "Gender Hash: Success"
    , "Compiling Gender Logarithm"
    , "Scanning Aura"
    , "Carrying the 1"
    , "Converting to Metric"
    , "Watching a YouTube video to remind myself how Pythagoras works"
    , "Defragmenting Gender Matrix"
    ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
