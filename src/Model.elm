module Model exposing (..)

import Http


type alias Model =
    { mode : AppMode
    , gender : Gender
    , points : Int
    , list : List String
    , loading_message : Maybe String
    , loading_counter : Int
    , form : Form
    }


type alias Gender =
    Maybe String


type AppMode
    = Default
    | Reveal


type Msg
    = NewGender Gender
    | NewPoints Int
    | SetLoadingMessage Int
    | Load
    | GetGenders
    | GotGenders (Result Http.Error String)
    | GenderSelected Int
    | NoAction
    | Update Model
    | UpdateForm Form
    | FormSubmit
    | ModeSwitch


type alias Form =
    { what : Maybe QuizWhatIsGender
    , which : Maybe String
    , born : Bool
    , unborn : Bool
    , unconceived : Bool
    }


type QuizWhatIsGender
    = A
    | Social
    | Construct
